package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextPane;

public class RegistroVista extends JFrame {

	private JPanel contentPane;
	public JTextField textField_1;
	public JTextField textField;
	public JButton btnNewButton;	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegistroVista frame = new RegistroVista();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegistroVista() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(45, 68, 150, 41);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNombre = new JLabel("Apellido");
		lblNombre.setBounds(45, 140, 46, 14);
		contentPane.add(lblNombre);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(45, 165, 150, 41);
		contentPane.add(textField_1);
		
		JLabel label = new JLabel("Nombre");
		label.setBounds(45, 42, 46, 14);
		contentPane.add(label);
		
		btnNewButton = new JButton("New button");
		btnNewButton.setBounds(276, 116, 89, 23);
		contentPane.add(btnNewButton);
	}
}
