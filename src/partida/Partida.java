package partida;

import java.util.*;
import java.awt.EventQueue;
import java.sql.*;

//import java.util.Arrays;
//import java.util.List;
//import java.util.Comparator;
//import java.util.Collections;
//import java.util.Set;
//import java.util.TreeSet;

/** La clase principal del juego
 * 
 * @author Oleg, Maria, Elizabeth
 *
 */
public class Partida {
	
	// Datos iniciales
	public static int idJugador = 1;
	public static int idNivel = 1;
	public static int cantidadVida = 3;
	public static String nombreUsuario = "Oleg";
	protected static String contrasena = "qwerty123";
	public static String email = "omikheles@fp.uoc.edu";
	public static String telefono = "600000004";
	
	
	public static int puntuacion;
	
	public static Utilidad utilidad = new Utilidad();
	public static Connection con = utilidad.conectar();
//	public static Statement stmt;
//	public static ResultSet rs;
	
	public static void main(String[] args) {												
		
		
		// Creamos la partida
		 crearPartida(idJugador,idNivel,cantidadVida);

		
		
//		Utilidad utilidad = new Utilidad();
//		con = utilidad.conectar();
		
//		String sql = "select * from usuarios";
//		
//		try {		
//			stmt = con.createStatement();
//			rs = stmt.executeQuery(sql);
//			while(rs.next()) {
//				System.out.println("Nombre jugador: " + rs.getString("nombre"));
//			}
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
	}

	/** Creamos la partida	 
	 * @param idJugador El id de Jugador
	 * @param idNivel El id de nivel
	 * @param cantidadVida Cantidad de vidas inicial
	 */
	public static void crearPartida(int idJugador, int idNivel, int cantidadVida) {
		
		// Creamos el objeto de ejemplo Jugador 1
		Jugador jugadorObject = new Jugador(nombreUsuario,contrasena,idJugador,0,idNivel);					
					          		
		// Creamos el objeto Vida con el valor inicial.
		Vida vidaObject = new Vida(cantidadVida);
		
		// Creamos el objeto Nivel.
		Nivel nivelObject = new Nivel(1,20,idNivel);
		
		// Creamos el objeto Monstruo
		Monstruo monstruoObject = new Monstruo(1348,10000,100,true);				
		
		// Registramos el jugador
		try {
			
			if(jugadorObject.registrar(nombreUsuario,contrasena,"test0103@gmail.com","600000000")) {	
				System.out.println("El Jugador "+idJugador+" ha sido registrado con nombre: "+nombreUsuario+"y contraseña:"+contrasena);				
			}
		
		} catch (Exception e) {
			// TODO Auto-generated catch block			
			e.printStackTrace();
		}
		
		// Login usuario
		System.out.println("\n");
		System.out.println("---------------------");
		System.out.println("Login de usuario por email y contraseña");
		System.out.println("---------------------");
		if(jugadorObject.login(email,contrasena)) {	
			System.out.println("El Jugador ha entrado correctamente");			
		} else {				
			System.out.println("Usuario no existe, comprueba el login y contraseña");
		}
		
		System.out.println("\n");
		System.out.println("---------------------");
		System.out.println("Recuperamos el ID de jugador");
		System.out.println("---------------------");		
		System.out.println(idJugador = jugadorObject.getJudadorId(email));
		
		
		System.out.println("\n");
		System.out.println("---------------------");
		System.out.println("Creamos la partida");
		System.out.println("---------------------");
		System.out.println("El id de Jugador es: " + idJugador);
		System.out.println("El id de Nivel es: " + idJugador);
		System.out.println("El jugador tiene: " + cantidadVida+ " vidas.");
		System.out.println("El nombre de jugador: " + nombreUsuario);
		System.out.println("La contrasena de jugador: " + contrasena);
				
		
		// Ejemplo para actualizar puntos
		System.out.println("\n");
		System.out.println("---------------------");
		System.out.println("Ejemplo para actualizar puntos");
		System.out.println("---------------------");
		System.out.println("Puntuacion inicial: "+jugadorObject.puntuacion);
		jugadorObject.actualizarPuntuacion(idJugador, 35000);				
		System.out.println("Puntuacion actualizada: "+jugadorObject.puntuacion);	
		
		
		// Ejemplo para actualizar nivel
		System.out.println("\n");
		System.out.println("---------------------");
		System.out.println("Ejemplo para actualizar nivel");
		System.out.println("---------------------");
		System.out.println("Nivel actual: "+jugadorObject.nivel);
		jugadorObject.actualizarNivel(1, 3);
		System.out.println("Nivel actualizado: "+jugadorObject.nivel);		
		
		// Ejemplo para editar datos de usuario
		System.out.println("\n");
		System.out.println("---------------------");
		System.out.println("Ejemplo para actualizar datos usuario");
		System.out.println("---------------------");
		jugadorObject.editarDatos(idJugador, nombreUsuario, contrasena, email,telefono);
		System.out.println("Usuario actualizado");		
		
		
		// Ejemplo para quitar o añadir vida
		System.out.println("\n");
		System.out.println("---------------------");
		System.out.println("Ejemplo para quitar o añadir vida");
		System.out.println("---------------------");
		System.out.println("El jugador tiene vidas: "+ cantidadVida);		
		System.out.println("Quitamos 1 vida, vidas actuales: "+vidaObject.quitarVida(1));		
		System.out.println("Añadimos 1 vida, vidas actuales: "+vidaObject.anadirVida(1));
		
		// Ejemplo crear adversario		
		System.out.println("\n");
		System.out.println("---------------------");
		System.out.println("Ejemplo crear adversario monstruo");
		System.out.println("---------------------");
		monstruoObject.crearAdversario();				
		
		// Ejemplo matar adversario
		System.out.println("\n");
		System.out.println("---------------------");
		System.out.println("Ejemplo crear adversario matar monstruo");
		System.out.println("---------------------");
		monstruoObject.matarAdversario();
		
		// Comparar estadísticas de los jugadores
		List<Jugador> jugadores = Arrays.asList(
			new Jugador("Oleg","pass123",1,25000,1),
			new Jugador("Eli","pass435",2,95000,4),
			new Jugador("Mamen","pass363",3,62000,3)
		);
			
		// Sorteamos el listado segun la logica de comparable
		Collections.sort(jugadores);
		
		System.out.println("\n");
		System.out.println("--------------------------------------");
		System.out.println("COMPARAR ESTADÍSTICAS DE LOS JUGADORES");
		System.out.println("--------------------------------------");
		System.out.println("NOMBRE		|PUNTOS		|NIVEL");
		System.out.println("--------------------------------------");
		for(Jugador jugador : jugadores) {
			System.out.println(jugador.nombreUsuario+"		|"+jugador.puntuacion+"		|"+jugador.nivel);
			System.out.println("--------------------------------------");
		}
		
		
//		System.out.println(jugadorObject.getIdPuntuacion(email));
//		System.out.println(jugadorObject.getPuntuacion(email));		
//		System.out.println(jugadorObject.getIdNivel(email));
		jugadorObject.actualizarPuntuacion(jugadorObject.getIdPuntuacion(email),70000);
		jugadorObject.actualizarNivel(2, 3);
		
	}
		
}