

package partida;

/** La clase de Nave con sus metodos corespondientes
 * 
 * @author Oleg, Maria, Elizabeth - Grupo M.O.E
 *
 */

public class Nave implements Adversario {
    
     //atributos
    
    public String color;
    public boolean estadoVidaNave = true;   
    
    /** Constructor Nave. Realizamos la conexion a la base de datos para cargar los datos de la partida
     * @param id El Id nave
	 * @param color El color de la nave
	 * @param estadoVidaNave devuelve el estado de la nave
	 */    
    public Nave (int id,String color, boolean estadoVidaNave){
           
        this.color = color;
        this.estadoVidaNave = estadoVidaNave;
            
    }
    
	@Override
	public int crearAdversario() {
		// TODO Auto-generated method stub		
		return 0;
	}

	@Override
	public boolean matarAdversario() {
		// TODO Auto-generated method stub
		return false;
	}
    
	/**
	 * Crear nave
	 * @param color el color de la nave
	 */
    public void crearNave (String color){        
       
    }
    
    /**
     * Matar nave
     * @param estadoVidaNave El estado de la vidad de nave
     * @return True o false
     */
    public boolean matarNave (boolean estadoVidaNave){
            
    	return estadoVidaNave;
       
    }
    
}
