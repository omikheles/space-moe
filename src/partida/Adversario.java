package partida;

	/** La clase de Adversario con sus metodos corespondientes
	 * 
	 * @author Oleg, Maria, Elizabeth - Grupo M.O.E
	 *
	 */
   	public interface Adversario {
  
    
   		// Metodos de la interfaz adversario 
   		public int crearAdversario();
    
   		public boolean matarAdversario();
    
    
}
