
package partida;

/** La clase de Monstruo con sus metodos corespondientes
 * 
 * @author Oleg, Maria, Elizabeth - Grupo M.O.E
 *
 */
public class Monstruo extends Partida implements Adversario{
    
   
    //Atributos    
    public int id = 0;
    public int puntos = 0;
    public int velocidadAtaque = 0;
    public boolean estadoVida = true;       
    
    /**
     * Constructor Monstruo
     * @param id El id monstruo
     * @param puntos Puntos
     * @param velocidadAtaque Velocidad de ataque
     * @param estadoVida Estado vida
     */
    public Monstruo(int id, int puntos, int velocidadAtaque, boolean estadoVida){
    
        this.id = id;
        this.puntos = puntos;
        this.velocidadAtaque = velocidadAtaque;
        this.estadoVida = estadoVida;        
       
    }
    
    /**
     * Crear adversario
     */
	public int crearAdversario() {

    	this.crearMonstruo(id, puntos, velocidadAtaque);    	
		return 0;
		
	}


	/**
	 * Matar adversario
	 */
	public boolean matarAdversario() {
		// TODO Auto-generated method stub
		this.matarMonstruo(id);		
		return false;
	}
	        
    
    /**
     * Crear monstruo
     * @param id el ID de monstruo
     * @param puntos Puntos por matar el monstruo
     * @param velocidadAtaque Velocidad de ataque
     * @return El ID de monstruo
     */
    public int crearMonstruo (int id, int puntos, int velocidadAtaque){
    	
    	System.out.println("El monstruo con el id: "+id+" esta creado");
    	return id;
       
	}
    
    /**
     * Matar monstruo
     * @param id el ID de monstruo
     * @return true o false
     */
    public boolean matarMonstruo (int id){
    	
    	System.out.println("El monstruo con el id: "+id+" esta muerto");
     
    	try {				
			return true;
		} catch (Exception e) {
			// Captamos cualquier error.
			return false;
		}
    	
       
    }
    
    
}
