package partida;

/** La clase de Vida
 * 
 * @author Oleg, Maria, Elizabeth - Grupo M.O.E
 *
 */
public class Vida extends Partida {
	
	public int cantidadVida;
	
	/**
	 * @param cantidadVida valor inicial
	 */
	public Vida(int cantidadVida) {
		this.cantidadVida = cantidadVida;		
	}
	
	/** Quitar vida	
	 * @param cantidadVida Cantidad vidas
	 * @return int El numero de vidas
	 */
	public int quitarVida(int cantidadVida) {
		
		this.cantidadVida -= cantidadVida; 		
		return this.cantidadVida;
		
	}
	
	/** Añadir vida 
	 * @param cantidadVida La cantidad de vidas
	 * @return int Cantidad de vidas
	 */
	public int anadirVida(int cantidadVida) {
		
		this.cantidadVida += cantidadVida; 		
		return this.cantidadVida;
		
	}


}
