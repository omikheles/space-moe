package partida;

import java.sql.*;

/** La clase de Jugador con sus metodos corespondientes
 * 
 * @author Oleg, Maria, Elizabeth - Grupo M.O.E
 *
 */
public class Jugador extends Partida implements Comparable<Jugador> {
	
	public String nombreUsuario;
	protected String contrasena;
	int id;
	int puntuacion;
	int nivel = 1;
	
	/**
	 * Constructor de objeto Jugador
	 * @param nombreUsuario El nombre de usuario 
	 * @param contrasena La contraseña
	 * @param id El id de Jugador
	 * @param puntuacion Puntos jugador
	 * @param nivel El nivel de jugador
	 */
	public Jugador(String nombreUsuario, String contrasena, int id, int puntuacion, int nivel) {
		this.nombreUsuario = nombreUsuario;
		this.contrasena = contrasena;
		this.id = id;
		this.puntuacion = puntuacion;
		this.nivel = nivel;		
	}
	
	/** Realizamos la conexion a la base de datos para registrar el usuario 
	 * @param nombreUsuario Nombre usuario
	 * @param contrasena Contraseña
	 * @param email E-mail
	 * @param telefono El telefono
	 * @return True o False
	 * @throws SQLException SQL Errors
	 */		
	public boolean registrar(String nombreUsuario, String contrasena, String email, String telefono) throws SQLException {							
						
		try (					
			PreparedStatement statement = con.prepareStatement("INSERT INTO  puntuaciones(puntuacion) VALUES (?)", Statement.RETURN_GENERATED_KEYS);			
			PreparedStatement statement2 = con.prepareStatement("INSERT INTO  Usuarios (nombre, password, email, telefono, nivelid,puntuacionid) VALUES (?, ?, ?, ?, ?, ?);	", Statement.RETURN_GENERATED_KEYS);
		)
		{
			
			con.setAutoCommit(false);					
			
			if(this.checkUsuarioExiste(email)) {
				throw new SQLException("REGISTRO: Usuario existe");
			}
			
			statement.setInt(1, 0);
			statement.executeUpdate();							
			
			try(ResultSet generatedKeys = statement.getGeneratedKeys()){
				if (generatedKeys.next()) {
					System.out.println("Id puntuacion: " + generatedKeys.getLong(1));	
					statement2.setString(1, nombreUsuario);
					statement2.setString(2, contrasena);
					statement2.setString(3, email);
					statement2.setString(4, telefono);
					statement2.setInt(5, 1);
					statement2.setLong(6, generatedKeys.getLong(1));
					statement2.executeUpdate();					
				} else {
					throw new SQLException("Failed");
				}
			}
			
			con.commit();														
			
		} catch (SQLException e) {
			
			con.rollback();			
			System.out.println(e.getMessage());
			
			return false;
		}
		
		return true;
		
	}
	
	/**
	 * Comprobamos si el usuario existe en la base
	 * @param email El e-mail de usuario
	 * @return true o false
	 */
	public boolean checkUsuarioExiste(String email) {
		
			
		try {		
			
			PreparedStatement statement = con.prepareStatement("SELECT email FROM usuarios WHERE email = ? LIMIT 1");
			statement.setString(1, email);
			ResultSet rs = statement.executeQuery();
			
			if(!rs.next()) {
				return false;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		
		return true;
				
	}
	
	/**
	 * Recuperamos el ID de jugador por email
	 * @param email El e-mail de jugador
	 * @return id El id de jugador
	 */
	public static int getJudadorId(String email){
		int id = 0;
		try {				
			
			PreparedStatement statement = con.prepareStatement("SELECT idUsuario FROM usuarios WHERE email = ? LIMIT 1");
			statement.setString(1, email);
			ResultSet rs = statement.executeQuery();
			
			if(rs.next()) {					
				id = rs.getInt("idUsuario");			
			}					
			
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return id;
	}
		
	
	/** Login de usuario
	 * @param email El email de usuario
	 * @param contrasena La contrasena de usuario
	 * @return True or false
	 */
	public boolean login(String email, String contrasena) {
					
		try {			
			
			PreparedStatement statement = con.prepareStatement("SELECT IdUsuario,email FROM usuarios WHERE email = ? AND password = ? LIMIT 1");
			statement.setString(1, email);
			statement.setString(2, contrasena);
			
			ResultSet rs = statement.executeQuery();
			
			if(rs.next()) {
				return true;
			}
			
		} catch (SQLException e) {
			// Captamos cualquier error.	
			e.printStackTrace();
		}
		
		return false;
		
	}
	
	/** Editar datos de usuario
	 * @param id El de usuario
	 * @param nombreUsuario El nombre de usuario
	 * @param contrasena La contrasena de usuario
	 * @param email El e-mail de usuario
	 * @param telefono El telefono de usuario
	 * @return True or false
	 */
	public boolean editarDatos(int id, String nombreUsuario, String contrasena, String email, String telefono) {
		
		try {
			
			PreparedStatement statement = con.prepareStatement("UPDATE usuarios SET nombre = ?, password = ?, email = ?, telefono = ? WHERE IdUsuario = ?");
			statement.setString(1, nombreUsuario);
			statement.setString(2, contrasena);
			statement.setString(3, email);
			statement.setString(4, telefono);
			statement.setInt(5, id);
			
			statement.executeUpdate();
			
			con.commit();
								
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	/**	Actualizar puntos
	 * @param id El de usuario
	 * @param puntuacion Puntos ganados
	 * @return True or false
	 */
	public boolean actualizarPuntuacion(int id, int puntuacion) {
		
		try {		
			
			PreparedStatement statement = con.prepareStatement("UPDATE puntuaciones SET puntuacion = ? WHERE IdPuntuacion = ?");			
			statement.setInt(1, puntuacion);
			statement.setInt(2, id);
			
			statement.executeUpdate();
			
			con.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
		
	}	
	
	/**
	 * Recuperamos el ID de puntuacion
	 * @param email El e-mail de usuario
	 * @return El id de puntuacion
	 */
	public int getIdPuntuacion(String email){
		int id = 0;
		try {				
			
			PreparedStatement statement = con.prepareStatement("SELECT puntuacionid FROM usuarios WHERE email = ? LIMIT 1");
			statement.setString(1, email);
			ResultSet rs = statement.executeQuery();
			
			if(rs.next()) {					
				id = rs.getInt("puntuacionid");			
			}					
			
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return id;
	}
	
	/**
	 * Recuperamos puntuacion de Jugador
	 * @param email El e-mail de usuario
	 * @return puntos Los puntos de usuario
	 */
	public int getPuntuacion(String email){
		int puntos = 0;
		try {				
			
			PreparedStatement statement = con.prepareStatement("SELECT puntuacion FROM puntuaciones WHERE IdPuntuacion = ? LIMIT 1");
			statement.setInt(1, this.getIdPuntuacion(email));
			ResultSet rs = statement.executeQuery();
			
			if(rs.next()) {					
				puntos = rs.getInt("puntuacion");			
			}					
			
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return puntos;
	}
	
	/** Actualizar el nivel
	 * @param id El id de nivel
	 * @param nivel El nivel
	 * @return True or false
	 */
	public boolean actualizarNivel(int id, int nivel) {		
		
		try {		
			
			PreparedStatement statement = con.prepareStatement("UPDATE nivel SET nivel = ? WHERE IdNivel = ?");			
			statement.setInt(1, nivel);
			statement.setInt(2, id);
			
			statement.executeUpdate();
			
			con.commit();
			
		} catch (Exception e) {
			// Captamos cualquier error.
			return false;
		}
		return true;
		
	}
	
	/**
	 * Recuperamos el ID de nivel
	 * @param email El e-mail de usuario
	 * @return nivelid El id de nivel
	 */
	public int getIdNivel(String email){
		int id = 0;
		try {				
			
			PreparedStatement statement = con.prepareStatement("SELECT nivelid FROM usuarios WHERE email = ? LIMIT 1");
			statement.setString(1, email);
			ResultSet rs = statement.executeQuery();
			
			if(rs.next()) {					
				id = rs.getInt("nivelid");			
			}					
			
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return id;
	}
	
	/** Eliminar jugador por el ID
	 * @param id El id de jugador
	 * @return True or false
	 */
	public boolean eliminarJugador(int id) {
		
		try {				
			return true;
		} catch (Exception e) {
			// Captamos cualquier error.
			return false;
		} 
		
	}
	
	/**
	 * Es un override de metodo compareTo con la logica de comparador para el listado de los Jugadores	 
	 * @return int El resultado para el objeto
	 */
	public int compareTo(Jugador o) {
	    
		int resultado = 0;
		
		// Comparamos por la cantidad de puntos
		if(o.puntuacion < this.puntuacion) {
			resultado = -1;
		}
	             
		return resultado;

	}

}
