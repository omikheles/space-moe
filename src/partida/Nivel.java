package partida;

/** La clase para construir el nivel
 * 
 * @author Oleg, Maria, Elizabeth
 *
 */
public class Nivel extends Partida {
	
	public int dificultadNivel;
	public int cantidadMonstruos;
	public int id;
	
	/** Constructor de Nivel
	 * @param dificultadNivel Dificultad de nivel
	 * @param cantidadMonstruos Cantidad de monstruos
	 * @param id El id de nivel
	 */
	public Nivel(int dificultadNivel, int cantidadMonstruos, int id) {
		
		this.dificultadNivel = dificultadNivel;
		this.cantidadMonstruos = cantidadMonstruos;
		this.id = id;
		
		//TODO Logica
		
	}

}
